## Jenis-jenis IP beserta kelasnya

| Kelas | Jumlah Jaringan | Range IP                        |
| ----- | --------------- | ------------------------------- |
| A     | 126             | 1.xxx.xxx.xxx - 126.xxx.xxx.xxx |
| B     | 128-191         | 128.0.xxx.xxx - 191.255.xxx.xxx |
| C     | 192-223         | 192.0.0.xxx - 255.255.255.xxx   |
| D     | 224-239         | 244.0.0.0 - 239.255.255.255     |
| E     | 240-255         | 240.0.0.0 - 254.255.255.255     |
